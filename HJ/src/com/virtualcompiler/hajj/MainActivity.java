package com.virtualcompiler.hajj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	GridView grid;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		actionBar = getSupportActionBar();

		setTitle(R.string.app_name);

		CustomGrid adapter = new CustomGrid(MainActivity.this,
				ContentHelper.web, ContentHelper.kidsImageId);

		grid = (GridView) findViewById(R.id.grid);
		grid.setAdapter(adapter);
		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// Launch ImageViewPager.java on selecting GridView Item
				Intent i = new Intent(getApplicationContext(),
						ImageViewPager.class);

				// Show a simple toast message for the item position
				Toast.makeText(MainActivity.this, "" + position,
						Toast.LENGTH_SHORT).show();

				// Send the click position to ImageViewPager.java using intent
				i.putExtra("id", position);

				// Start ImageViewPager
				startActivity(i);

				// Toast.makeText(MainActivity.this,
				// "You Clicked at " + web[+position], Toast.LENGTH_SHORT)
				// .show();

			}
		});

	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return super.onCreateOptionsMenu(menu);
	}
}
