package com.virtualcompiler.hajj;

import java.util.List;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ImagePagerAdapter extends PagerAdapter {

	private List<ImageView> images;
	List<TextView> textList;

	public ImagePagerAdapter(List<ImageView> images, List<TextView> textList) {

		this.images = images;
		this.textList = textList;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		
		
//		layout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				Toast.makeText(MainActivity.this, "Page " + page + " clicked",
//						Toast.LENGTH_LONG).show();
//			}
//		});

	
		// ImageView imageView = images.get(position);
		// TextView textView = textList.get(position);
		//
		// container.addView(imageView);
		// container.addView(textView);

		return container;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(images.get(position));
	}

	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object o) {
		return view == o;
	}
}
