package com.virtualcompiler.hajj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

public class ImageViewPager extends ActionBarActivity {
	// Declare Variable
	int position;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set title for the ViewPager
		// setTitle("ViewPager");
		// Get the view from view_pager.xml
		setContentView(R.layout.view_pager);

		setTitle(R.string.app_name);

		// Retrieve data from MainActivity on item click event
		Intent p = getIntent();
		position = p.getExtras().getInt("id");

		// Set the images into ViewPager
		ViewPager viewpager = (ViewPager) findViewById(R.id.pager);

		// shihab edit
		ViewPagerAdapter adapter = new ViewPagerAdapter(this,
				ContentHelper.web, ContentHelper.web, ContentHelper.web,
				ContentHelper.kidsImageId, ContentHelper.duaImageId);
		// Binds the Adapter to the ViewPager
		viewpager.setAdapter(adapter);
		viewpager.setCurrentItem(position);

	}

}